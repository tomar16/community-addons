# -*- coding: utf-8 -*-

from . import installer
from . import ir_model
from . import ir_translation
from . import report
